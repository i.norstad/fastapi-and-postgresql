from fastapi import APIRouter, Depends, Response
from queries.vacations import VacationIn, VacationRepo, VacationOut, Error
from typing import Union, List, Optional

router = APIRouter()

# CREATE VACATION
@router.post("/vacations", response_model=Union[VacationOut, Error])
def create_vacation(
    vacation: VacationIn,
    response: Response,
    repo: VacationRepo = Depends()
):
    # print('vacation', vacation)
    # print('from_date', vacation.from_date.month)
    # print(repo)
    response.status_code=400
    return repo.create(vacation)


# GET LIST OF VACATIONS
@router.get("/vacations/", response_model=Union[List[VacationOut], Error])
def get_all(
    repo: VacationRepo = Depends()
):
    return repo.get_all()

# UPDATE VACATION
@router.put("/vacations/{vacation_id}", response_model=Union[VacationOut, Error])
def update_vacation(
    vacation_id: int,
    vacation: VacationIn,
    repo: VacationRepo = Depends(),
) -> Union[VacationOut, Error]:
    return repo.update(vacation_id, vacation)

# DELETE VACATION
@router.delete("/vacations/{vacation_id}", response_model=bool)
def delete_vacation(
    vacation_id: int,
    repo: VacationRepo = Depends(),
) -> bool:
    return repo.delete_vacation(vacation_id)

# GET A SINGLE VACATION
@router.get("/vacations/{vacation_id}", response_model=Optional[VacationOut])
def get_one_vacation(
    vacation_id: int,
    response: Response,
    repo: VacationRepo = Depends(),
) -> VacationOut:
    vacation = repo.get_one(vacation_id)
    if vacation is None:
        response.status_code=404
    return vacation
